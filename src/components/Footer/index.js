import React from 'react';
import './footer.css';

const Footer = () => (
  <footer className="footer fix-footer">
    <div className="columns">
      <div className="column">
        <p className="has-text-left">v 0.2</p>
      </div>
      <div className="column">
        <p className="has-text-centered">faturatasarla.io</p>
      </div>
      <div className="column">
        <p className="has-text-right">
          Made with <span style={{ color: 'red' }}>&#10084;</span>
        </p>
      </div>
    </div>
  </footer>
);

export default Footer;
