import React from 'react';
import PropTypes from 'prop-types';
import './ResizablePanel.css';

const getWidth = () =>
  Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth,
  );
class ResizablePanel extends React.Component {
  constructor(props) {
    super(props);
    this.container = React.createRef();
    this.enableResizing = this.enableResizing.bind(this);
    this.disableResizing = this.disableResizing.bind(this);
    this.resizeToolbox = this.resizeToolbox.bind(this);
    this.state = {
      isResizing: false,
      width: 250,
    };
  }

  componentDidMount() {
    document.body.addEventListener('mousemove', this.resizeToolbox);
    document.body.addEventListener('mouseup', this.disableResizing);
  }
  componentWillUnmount() {
    document.body.removeEventListener('mousemove', this.resizeToolbox);
    document.body.addEventListener('mouseup', this.disableResizing);
  }

  resizeToolbox(e) {
    if (!this.state.isResizing) return;

    this.setState({
      width: this.props.handlerPosition === 'right' ? e.clientX : getWidth() - e.clientX,
    });
  }

  enableResizing() {
    this.setState({
      isResizing: true,
    });
  }
  disableResizing() {
    this.setState({
      isResizing: false,
    });
  }

  render() {
    return (
      <div
        ref={this.container}
        className={this.props.className}
        style={{ width: this.state.width, position: 'relative' }}
      >
        <div
          role="presentation"
          onMouseDown={this.enableResizing}
          onMouseUp={this.disableResizing}
          className={`resizehadler resizehandler-${this.props.handlerPosition}`}
        />
      </div>
    );
  }
}

ResizablePanel.propTypes = {
  className: PropTypes.string,
  handlerPosition: PropTypes.string,
};

ResizablePanel.defaultProps = {
  className: 'app-panel',
  handlerPosition: 'right',
};
export default ResizablePanel;
