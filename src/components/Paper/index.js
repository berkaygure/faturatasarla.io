import React from 'react';
import { connect } from 'react-redux';
import Horizontal from './Ruler/Horizontal';
import Vertical from './Ruler/Vertical';
import './Paper.css';

class Paper extends React.Component {
  constructor(props) {
    super(props);
    this.paperContainer = React.createRef();
    this.paper = React.createRef();
  }

  componentDidMount() {
    const outerWidth = this.paperContainer.current.offsetWidth;
    const innerWidth = this.paper.current.offsetLeft + this.paper.current.clientWidth;

    this.paperContainer.current.scrollLeft = (innerWidth - outerWidth) / 2;
  }

  render() {
    return (
      <div ref={this.paperContainer} className="main">
        <div className="paperWrapper" style={{ width: 800, height: 900 }}>
          {this.props.windows.ruler.visible ? (
            <Horizontal followerX={10} followerWidth={100} show width={800} />
          ) : null}
          {this.props.windows.ruler.visible ? (
            <Vertical followerY={10} followerHeight={100} show height={900} />
          ) : null}
          <div ref={this.paper} className="paper">
            Paper
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  windows: state.windows,
});

export default connect(
  mapStateToProps,
  {},
)(Paper);
