import React from 'react';
import PropTypes from 'prop-types';
import './Ruler.css';

const HorizontalRulerTick = props => (
  <div className="rulerHTick" style={{ width: 32, left: 32 * props.i }}>
    <div className="rulerHNumber">{props.i}</div>
  </div>
);

const Horizontal = (props) => {
  const HorizontalRulerTicks = [];
  for (let i = 0; i < props.width / 32; i += 1) {
    HorizontalRulerTicks.push(<HorizontalRulerTick key={i} i={i} />);
  }
  return (
    <div className="ruler horizontal">
      {HorizontalRulerTicks}
      {props.show ? (
        <div
          className="rulerFollwerH"
          style={{
            display: 'block',
            left: props.followerX,
            width: props.followerWidth,
          }}
        />
      ) : null}
    </div>
  );
};

HorizontalRulerTick.propTypes = {
  i: PropTypes.number,
};

HorizontalRulerTick.defaultProps = {
  i: 0,
};

Horizontal.propTypes = {
  width: PropTypes.number,
  show: PropTypes.bool,
  followerX: PropTypes.number,
  followerWidth: PropTypes.number,
};

Horizontal.defaultProps = {
  width: 0,
  show: false,
  followerX: 0,
  followerWidth: 0,
};

export default Horizontal;
