import React from 'react';
import PropTypes from 'prop-types';
import './Ruler.css';

const VerticalRulerTick = props => (
  <div className="rulerVTick" style={{ height: 32, top: 32 * props.i }}>
    <div className="rulerVNumber">{props.i}</div>
  </div>
);

const Vertical = (props) => {
  const VerticalRulerTicks = [];
  for (let i = 0; i < props.height / 32; i += 1) {
    VerticalRulerTicks.push(<VerticalRulerTick key={i} i={i} />);
  }
  return (
    <div className="ruler vertical">
      {VerticalRulerTicks}
      {props.show ? (
        <div
          className="rulerFollwerV"
          style={{ display: 'block', top: props.followerY, height: props.followerHeight }}
        />
      ) : null}
    </div>
  );
};

VerticalRulerTick.propTypes = {
  i: PropTypes.number,
};

VerticalRulerTick.defaultProps = {
  i: 0,
};

Vertical.propTypes = {
  height: PropTypes.number,
  show: PropTypes.bool,
  followerY: PropTypes.number,
  followerHeight: PropTypes.number,
};

Vertical.defaultProps = {
  height: 0,
  show: false,
  followerY: 0,
  followerHeight: 0,
};

export default Vertical;
