import React from 'react';
import ResiazblePanel from '../ResizablePanel';
import './Toolbox.css';

const Toolbox = () => <ResiazblePanel className="app-panel toolbox" />;

export default Toolbox;
