import Header from './Header';
import Footer from './Footer';
import Paper from './Paper';
import Properties from './Properties';
import Toolbox from './Toolbox';

export { Header, Paper, Toolbox, Properties, Footer };
