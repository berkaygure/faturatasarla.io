import React from 'react';
import PropTypes from 'prop-types';
import './Items.css';

const DropdownItem = props => (
  <button className="dropdown-item" onMouseDown={props.onClick}>
    <span className="icon">
      <i className={props.icon ? props.icon : 'mdi'} />
    </span>
    {props.text}
  </button>
);

DropdownItem.propTypes = {
  text: PropTypes.string,
  icon: PropTypes.string,
  onClick: PropTypes.func,
};

DropdownItem.defaultProps = {
  text: '',
  icon: '',
  onClick: null,
};

export default DropdownItem;
