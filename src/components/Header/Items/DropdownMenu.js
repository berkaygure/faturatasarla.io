import React from 'react';
import PropTypes from 'prop-types';

class DropdownMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false,
    };
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.focusOut = this.focusOut.bind(this);
  }

  toggleDropdown() {
    this.setState({
      isActive: !this.state.isActive,
    });
  }

  focusOut() {
    this.setState({
      isActive: false,
    });
  }

  render() {
    return (
      <div
        onBlur={this.focusOut}
        className={`navbar-item has-dropdown ${this.state.isActive ? 'is-active' : ''}`}
      >
        <button onClick={() => this.toggleDropdown()} className="navbar-link" href="b.com">
          {this.props.title}
        </button>
        <div className="navbar-dropdown is-boxed">{this.props.children}</div>
      </div>
    );
  }
}

DropdownMenu.propTypes = {
  title: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

DropdownMenu.defaultProps = {
  children: null,
  title: '',
};

export default DropdownMenu;
