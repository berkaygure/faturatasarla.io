import React from 'react';
import PropTypes from 'prop-types';
import DropdownItem from './DropdownItem';
import './Items.css';

class CheckedDropdownItem extends React.Component {
  state = {
    checked: false,
  };

  render() {
    return (
      <DropdownItem
        onClick={this.props.onClick}
        text={this.props.text}
        icon={this.props.checked ? 'mdi mdi-check' : ''}
      />
    );
  }
}

CheckedDropdownItem.propTypes = {
  text: PropTypes.string,
  checked: PropTypes.bool,
  onClick: PropTypes.func,
};

CheckedDropdownItem.defaultProps = {
  text: '',
  checked: false,
  onClick: null,
};

export default CheckedDropdownItem;
