import React from 'react';
import PropTypes from 'prop-types';

const MenuItem = props => (
  <button onMouseDown={props.onClick} className="navbar-item">
    {props.icon ? (
      <span className="icon">
        <i className={props.icon} />
      </span>
    ) : null}
    {props.title}
  </button>
);

MenuItem.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  onClick: PropTypes.func,
};

MenuItem.defaultProps = {
  title: '',
  icon: '',
  onClick: index => index,
};

export default MenuItem;
