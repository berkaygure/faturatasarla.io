import React from 'react';
import PropTypes from 'prop-types';
import './Items.css';

class DropdownButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false,
    };
    this.wrapper = React.createRef();
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.focusOut = this.focusOut.bind(this);
    this.onSelect = this.onSelect.bind(this);
  }

  onSelect(index) {
    if (this.props.onSelect !== undefined) {
      this.props.onSelect(index);
    }
  }

  toggleDropdown() {
    this.setState({
      isActive: !this.state.isActive,
    });
  }

  focusOut() {
    this.setState({
      isActive: false,
    });
  }

  render() {
    const { children } = this.props;
    const childrenWithProps = React.Children.map(children, (child, index) =>
      React.cloneElement(child, {
        onClick: () => {
          this.onSelect(index);
        },
      }));

    return (
      <div
        onBlur={this.focusOut}
        className={`dropdown ${this.state.isActive ? 'is-active' : ''}`}
      >
        <div className="dropdown-trigger">
          <button
            className="button"
            aria-haspopup="true"
            aria-controls="dropdown-menu"
            onClick={this.toggleDropdown}
          >
            {this.props.text}
            <span className="icon is-small">
              <i className="mdi mdi-menu-down" aria-hidden="true" />
            </span>
          </button>
        </div>
        <div className="dropdown-menu" id="dropdown-menu" role="menu">
          <div className="dropdown-content">{childrenWithProps}</div>
        </div>
      </div>
    );
  }
}

DropdownButton.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  onSelect: PropTypes.func,
};
DropdownButton.defaultProps = {
  children: null,
  text: '',
  onSelect: index => index,
};

export default DropdownButton;
