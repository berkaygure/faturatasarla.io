import React from 'react';
import './AppBar.css';
import MenuItem from './Items/MenuItem';
import DropdownMenu from './Items/DropdownMenu';

const logo = require('./../../assets/img/logo/short.png');

const AppBar = () => (
  <nav className="navbar" aria-label="main navigation">
    <div className="navbar-brand">
      <a className="navbar-item" href={process.env.REACT_APP_HOME}>
        <img className="logo" src={logo} alt="Fatura Tasarla" />
      </a>
    </div>
    <div className="navbar-menu-wrapper">
      <div>
        <a className="app-name" href="http://a.com">
          Başlıksız Fatura
        </a>
      </div>
      <div className="navbar-menu app-menu">
        <DropdownMenu title="Dosya">
          <MenuItem icon="mdi mdi-file" title="Yeni..." />
          <MenuItem icon="mdi mdi-folder-open" title="Aç..." />
          <MenuItem icon="mdi mdi-history" title="Son projeler..." />
          <hr className="navbar-divider" />
          <MenuItem icon="mdi mdi-content-save" title="Kaydet" />
          <MenuItem icon="mdi mdi-content-save-settings" title="Farklı Kaydet..." />
          <hr className="navbar-divider" />
          <MenuItem icon="mdi mdi-rename-box" title="İsmini Değiştir..." />
          <MenuItem icon="mdi mdi-file-export" title="Dışa Aktar..." />
          <MenuItem icon="mdi mdi-file-find" title="Önizle..." />
          <hr className="navbar-divider" />
          <MenuItem icon="mdi mdi-close-circle" title="Çıkış" />
        </DropdownMenu>
        <DropdownMenu title="Düzenle">
          <MenuItem icon="mdi mdi-undo" title="Geri Al" />
          <MenuItem icon="mdi mdi-redo" title="Yinele" />
          <hr className="navbar-divider" />
          <MenuItem icon="mdi mdi-content-cut" title="Kes" />
          <MenuItem icon="mdi mdi-content-copy" title="Kopyala" />
          <MenuItem icon="mdi mdi-content-paste" title="Yapıştır" />
          <MenuItem icon="mdi mdi-delete" title="Sil" />
        </DropdownMenu>
        <DropdownMenu title="Görünüm">
          <MenuItem title="Cetvel" />
        </DropdownMenu>
        <MenuItem title="Yardım" />
      </div>
    </div>
    <div className="navbar-menu">
      <div className="navbar-end">
        <div className="navbar-item">
          <div className="field is-grouped">
            <p className="control">
              <a
                className="button is-link"
                href="https://github.com/jgthms/bulma/releases/download/0.7.1/bulma-0.7.1.zip"
              >
                <span className="icon">
                  <i className="mdi mdi-play-circle-outline" />
                </span>
                <span>Önizle</span>
              </a>
            </p>
            <p className="control">
              <a
                className="button is-link"
                href="https://github.com/jgthms/bulma/releases/download/0.7.1/bulma-0.7.1.zip"
              >
                <span className="icon">
                  <i className="mdi mdi-play-circle-outline" />
                </span>
                <span>Önizle</span>
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </nav>
);

export default AppBar;
