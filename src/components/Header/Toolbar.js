import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { setWindowProperty } from '../../redux/actions/windowsAction';
import './Toolbar.css';
import CheckedDropdownItem from './Items/CheckedDropdownItem';
import DropdownButton from './Items/DropdownButton';

class Toolbar extends React.Component {
  constructor(props) {
    super(props);
    this.changeView = this.changeView.bind(this);
  }

  changeView(position) {
    const {
      appbar, toolbar, ruler, properties, setWindowProperty,
    } = this.props;
    switch (position) {
      case 0:
        setWindowProperty('properties', {
          visible: !properties.visible,
        });
        break;
      case 1:
        setWindowProperty('toolbar', {
          visible: !toolbar.visible,
        });
        break;
      case 2:
        setWindowProperty('appbar', {
          visible: !appbar.visible,
        });
        break;
      case 3:
        setWindowProperty('ruler', {
          visible: !ruler.visible,
        });
        break;
      default:
        break;
    }
  }

  render() {
    const {
      appbar, toolbar, ruler, properties,
    } = this.props;
    return (
      <div className="toolbar">
        <DropdownButton onSelect={this.changeView} text={<i className="mdi mdi-view-dashboard" />}>
          <CheckedDropdownItem checked={properties.visible} text="Özellikler Paneli" />
          <CheckedDropdownItem checked={toolbar.visible} text="Araç Çubuğu" />
          <CheckedDropdownItem checked={appbar.visible} text="Menü Çubuğu" />
          <CheckedDropdownItem checked={ruler.visible} text="Cetvel" />
        </DropdownButton>
        <span className="toolbar-split" />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    appbar, toolbar, ruler, properties,
  } = state.windows;
  return {
    appbar,
    toolbar,
    ruler,
    properties,
  };
};

Toolbar.propTypes = {
  appbar: PropTypes.shape({}),
  toolbar: PropTypes.shape({}),
  ruler: PropTypes.shape({}),
  properties: PropTypes.shape({}),
};

Toolbar.defaultProps = {
  appbar: null,
  toolbar: null,
  ruler: null,
  properties: null,
};

export default connect(
  mapStateToProps,
  { setWindowProperty },
)(Toolbar);
