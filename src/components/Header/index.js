import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AppBar from './AppBar';
import Toolbar from './Toolbar';

const Header = (props) => {
  const { appbar, toolbar } = props;
  return (
    <header>
      {appbar.visible ? <AppBar /> : null}
      {toolbar.visible ? <Toolbar /> : null}
    </header>
  );
};

const mapStateToProps = (state) => {
  const { appbar, toolbar } = state.windows;
  return { appbar, toolbar };
};

Header.propTypes = {
  appbar: PropTypes.shape({}),
  toolbar: PropTypes.shape({}),
};

Header.defaultProps = {
  appbar: null,
  toolbar: null,
};

export default connect(
  mapStateToProps,
  {},
)(Header);
