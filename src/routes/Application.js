import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Header, Properties, Toolbox, Paper, Footer } from '../components';

const Application = (props) => {
  const { tools, properties } = props;
  return (
    <div>
      <Header />
      <div className="application">
        {tools.visible ? <Toolbox /> : null}
        <Paper />
        {properties.visible ? <Properties /> : null}
      </div>
      <Footer />
    </div>
  );
};

const mapStateToProps = (state) => {
  const { tools, properties } = state.windows;
  return {
    tools,
    properties,
  };
};

Application.propTypes = {
  tools: PropTypes.shape({}),
  properties: PropTypes.shape({}),
};

Application.defaultProps = {
  tools: null,
  properties: null,
};

export default connect(
  mapStateToProps,
  {},
)(Application);
