import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Application from './routes/Application';
import './assets/css/App.css';

require('dotenv').config();

const _ = require('lodash');

window._ = _;

const App = () => (
  <div className="App">
    <Switch>
      <Route exact path="/" component={Application} />
    </Switch>
  </div>
);

export default App;
