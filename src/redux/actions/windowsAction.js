import { SET_WINDOW_PROPERTIES, ACTIVE_ITEM } from './types';

export const setWindowProperty = (property, value) => ({
  type: SET_WINDOW_PROPERTIES,
  payload: { property, value },
});

export const setWindowProperties = property => ({
  type: ACTIVE_ITEM,
  payload: { property },
});
