import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import AppReducer from './AppReducer';
import WindowsReducer from './WindowsReducer';

export default combineReducers({
  router: routerReducer,
  app: AppReducer,
  windows: WindowsReducer,
});
