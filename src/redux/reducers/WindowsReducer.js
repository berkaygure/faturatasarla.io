import { SET_WINDOW_PROPERTIES } from '../actions/types';

const WINDOWS = {
  tools: {
    visible: true,
    width: 200,
  },
  properties: {
    visible: true,
    width: 200,
  },
  toolbar: { visible: true },
  appbar: { visible: true },
  ruler: { visible: true },
};

export default (state = WINDOWS, action) => {
  switch (action.type) {
    case SET_WINDOW_PROPERTIES: {
      const property = action.payload.property;
      const value = action.payload.value;
      return {
        ...state,
        [property]: {
          ...state[property],
          ...value,
        },
      };
    }
    default:
      return state;
  }
};
